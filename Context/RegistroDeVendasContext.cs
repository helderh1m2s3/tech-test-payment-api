using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Context
{
    public class RegistroDeVendasContext : DbContext
    {
        public RegistroDeVendasContext(DbContextOptions<RegistroDeVendasContext> options) : base(options) 
        {

        }
        
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Vendedor> Vendedor { get; set; }
        public DbSet<Produto> Produtos { get; set; }

    }
}