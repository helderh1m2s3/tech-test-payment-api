using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Facade
{
    public class Venda
    {
        public class Request 
        {
            public class CriarVenda
            {
                public Vendedor Vendedor { get;set; }
                public DateTime Data { get; set; }
                public List<Produto> Produtos{ get; set; } = new List<Produto>();
            }

            public class AlterarStatus
            {
                public int IdVenda { get;set; }
                public EnumStatusVenda Status { get; set; }
            }
        }

        public class Response
        {
            public class Venda
            {
                public int IdVenda { get; set; }
                public DateTime Data { get; set; }
                public EnumStatusVenda Status { get; set; }
                public Vendedor Vendedor { get; set; } = new Vendedor();
                [Required]
                public List<Produto> Produtos{ get; set; } = new List<Produto>();

            }
        }

        public class Produto
        {
            public string NomeProduto {get; set;}
        }

        public class Vendedor 
        {
            public string Nome {get;set;}
            public string Cpf {get; set;}
            public string Email {get; set;}
            public string Telefone {get; set;}
        }
    }
}