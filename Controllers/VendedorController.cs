using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {   //Codigos comentados por não compor a solicitação do projeto
    
        // private readonly RegistroDeVendasContext _context;

        // public VendedorController(RegistroDeVendasContext context)
        // {
        //     _context = context;
        // } 

        // [HttpPost("RegistrarVendedor")]
        // public IActionResult CriarVendedor(Vendedor vendedor)
        // {
        //     if (vendedor.NomePessoa == null)
        //         {
        //             return BadRequest(new {Erro = "Todo Vendedor precisa ter ao menos um Nome"});
        //         }

        //     _context.Add(vendedor);
        //     _context.SaveChanges();

        //     return Ok(vendedor);
        // }

        // [HttpGet("ObterVendedorPor{id}")]
        // public IActionResult ObterVendedorPorId(int id)
        // {
        //     var vendedor = _context.Vendedor.Find(id);

        //     if (vendedor == null)
        //     {
        //         return NotFound();
        //     }
        //     return Ok(vendedor);
        // }

        // [HttpPut("AtualizarVendedorPor{id}")]
        // public IActionResult AtualizarVendedor(int id, Vendedor vendedor)
        // {
        //     var vendedorBanco = _context.Vendedor.Find(id);

        //         if (vendedorBanco == null)
        //             return NotFound();

        //         vendedorBanco.NomePessoa = vendedor.NomePessoa;
        //         vendedorBanco.Cpf = vendedor.Cpf;
        //         vendedorBanco.Email = vendedor.Email;
        //         vendedorBanco.Telefone = vendedor.Telefone;

        //         _context.Vendedor.Update(vendedorBanco);
        //         _context.SaveChanges();
            
        //     return Ok(vendedorBanco);
        // }
    }
}