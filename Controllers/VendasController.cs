using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly RegistroDeVendasContext _context;
        public VendasController(RegistroDeVendasContext context)
        {
            _context = context;
        }

        [HttpGet("ObterPor{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null)
                return NotFound();

            var vendedor = _context.Pessoas.Find(venda.IdVendedor);
            if(vendedor == null)
                return NotFound();

            var produtos = _context.Produtos.Where(i => i.IdVenda == venda.IdVenda).ToList();


            Facade.Venda.Response.Venda fVenda = new Facade.Venda.Response.Venda()
            {
                IdVenda = venda.IdVenda,
                Data = venda.Data,
                Status = venda.Status,
                Vendedor = new Facade.Venda.Vendedor()
                {
                    Nome = vendedor.NomePessoa,
                    Cpf = vendedor.Cpf,
                    Email = vendedor.Email,
                    Telefone = vendedor.Telefone
                }
            };

            foreach(Produto produto in produtos)
            {
                fVenda.Produtos.Add(new Facade.Venda.Produto(){ NomeProduto = produto.NomeProduto });
            }
            
            return Ok(fVenda);
        }

        [HttpPost("CriarVenda")]
        public IActionResult CriarVenda(Facade.Venda.Request.CriarVenda venda)
        {
            
            List<string> erros = new List<string>();
            if(venda != null){
                if(venda.Vendedor != null 
                    && venda.Vendedor.Nome.Length > 0 
                    && venda.Vendedor.Cpf.Length > 0
                    && venda.Vendedor.Email.Length > 0 
                    && venda.Vendedor.Telefone.Length > 0)
                {
                    if(venda.Produtos.Count == 0)
                    {
                        erros.Add("Informe no mínimo um Produto vendido."); 
                    }
                }else{ 
                    erros.Add("Informe os dados do vendedor: Nome, Cpf, Email e Telefone."); 
                }
            }else{ 
                erros.Add("Parâmetros não informados."); 
            }


            if(erros.Count > 0)
            {
                return BadRequest(new { Erro = string.Join(", ", erros) });
            }else{
                
                Venda vendaBanco = new Venda()
                {
                    Data = venda.Data,
                    Status = Models.EnumStatusVenda.AguardandoPagamento,
                    Vendedor = new Vendedor()
                    {
                        NomePessoa = venda.Vendedor.Nome,
                        Cpf = venda.Vendedor.Cpf.Replace(" ","").Replace(".","").Replace("-",""),
                        Email = venda.Vendedor.Email,
                        Telefone = venda.Vendedor.Telefone
                    }
                };

                _context.Vendas.Add(vendaBanco);
                _context.SaveChanges();

                foreach(Facade.Venda.Produto produto in venda.Produtos)
                {
                    _context.Produtos.Add(new Produto()
                    {
                        NomeProduto = produto.NomeProduto,
                        IdVenda = vendaBanco.IdVenda
                    });
                }
                _context.SaveChanges();

                return Ok(vendaBanco.IdVenda);
            }
        }

        [HttpPost("AlterarStatusDaVenda")]
        public IActionResult AlterarStatusDaVenda(Facade.Venda.Request.AlterarStatus request)
        {
            var venda = _context.Vendas.Find(request.IdVenda);
            if(venda == null)
                return NotFound("Id não encontrado");

            switch(request.Status)
            {
                case Models.EnumStatusVenda.PagamentoAprovado:
                    if(venda.Status.Equals(1))
                    {
                        return BadRequest(new { Erro = "O status atual precisa ser [Aguardando pagamento] para poder ser alterada para este novo status"});
                    }else{
                        venda.Status = request.Status;
                    }
                break;
                case Models.EnumStatusVenda.EnviadoParaTransportadora:
                    if(venda.Status.Equals(2))
                    {
                        return BadRequest(new { Erro = "O status atual precisa ser [Pagamento aprovado] para poder ser alterada para este novo status"});
                    }else{
                        venda.Status = request.Status;
                    }
                break;
                case Models.EnumStatusVenda.Entregue:
                    if(venda.Status.Equals(3))
                    {
                        return BadRequest(new { Erro = "O status atual precisa ser [Enviado para a transportadora] para poder ser alterada para este novo status"});
                    }else{
                        venda.Status = request.Status;
                    }
                break;
                case Models.EnumStatusVenda.Cancelada:
                    if(venda.Status.Equals(4))
                    {
                        return BadRequest(new { Erro = "Como esta venda já está na transportadora, ela não pode ser cancelada"});
                    }else{
                        venda.Status = request.Status;
                    }
                break;
                default:
                    return BadRequest(new { Erro = "Informe um estatus válido: [Pagamento aprovado, Enviado para a transportadora, Entregue ou Cancelada]"});
            }

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        
    }
}