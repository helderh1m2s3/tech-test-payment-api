using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PessoaController : ControllerBase
    {   //Codigos comentados por não compor a solicitação do projeto
        // private readonly RegistroDeVendasContext _context;

        // public PessoaController(RegistroDeVendasContext context)
        // {
        //     _context = context;
        // } 

        // [HttpPost("RegistrarPessoa")]
        // public IActionResult CriarPessoa(Pessoa pessoa)
        // {
        //     if (pessoa.NomePessoa == null)
        //         {
        //             return BadRequest(new {Erro = "Toda Pessoa precisa ter ao menos um Nome"});
        //         }

        //     _context.Add(pessoa);
        //     _context.SaveChanges();

        //     return Ok(pessoa);
        // }
       
        // [HttpGet("ObterPessoaPor{id}")]
        // public IActionResult ObterPessoaPorId(int id)
        // {
        //     var pessoa = _context.Pessoas.Find(id);

        //     if (pessoa == null)
        //     {
        //         return NotFound();
        //     }
        //     return Ok(pessoa);
        // }

        // [HttpPut("AtualizarPessoaPor{id}")]
        // public IActionResult AtualizarPessoa(int id, Pessoa pessoa)
        // {
        //     var pessoaBanco = _context.Pessoas.Find(id);

        //         if (pessoaBanco == null)
        //             return NotFound();

        //         pessoaBanco.NomePessoa = pessoa.NomePessoa;
        //         pessoaBanco.Cpf = pessoa.Cpf;
        //         pessoaBanco.Email = pessoa.Email;
        //         pessoaBanco.Telefone = pessoa.Telefone;

        //         _context.Pessoas.Update(pessoaBanco);
        //         _context.SaveChanges();
            
        //     return Ok(pessoaBanco);
        // }
    }
}