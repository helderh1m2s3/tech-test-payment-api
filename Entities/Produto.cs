using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Entities
{
    public class Produto
    {
        [Key()]
        public int IdProduto { get; set; }
        [Required]
        public string NomeProduto { get; set; }

        [ForeignKey("FK_Venda")]
        public int IdVenda {get; set;}
    }
}