using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        [Key()]
        public int IdVenda { get; set; }
        [Required]
        public DateTime Data { get; set; }
        [Required]
        public EnumStatusVenda Status { get; set; }
        [Required]
        public List<Produto> Produtos{ get; set; }

        [ForeignKey("Vendedor")]
        public int IdVendedor{ get; }
        public Vendedor Vendedor{ get; set; }

    }
}