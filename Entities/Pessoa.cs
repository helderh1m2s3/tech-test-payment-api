using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public class Pessoa
    {   
        [Key()]
        public int IdPessoa { get; set; }
        [Required]
        public string NomePessoa { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public string Telefone { get; set; }
        [Required]
        public string Email { get; set; }
       
    }
}